package fnac;

public class Disque extends Article {
	
	private String label;

	public Disque(String designation, int prix, String label) {
		super(designation, prix);
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "Disque [label=" + label + ", designation=" + designation + ", prix=" + prix + "]";
	}
	
	public void ecouter() {
		System.out.println("Disque en cours de lecture!");
	}

}
