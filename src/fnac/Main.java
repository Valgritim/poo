package fnac;

public class Main {

	public static void main(String[] args) {
		
		Article poche1 = new Poche("Symfony 3", 45, "9782409011740", 450, "informatique");		
		System.out.println(poche1);
		poche1.acheter();
		System.out.println("---------------------------------");
		
		Article livre1 = new Livre("Manuel PHP", 40,"9782409011810",500 );
		System.out.println(livre1);
		livre1.acheter();
		System.out.println("---------------------------------");
		
		Article video1 = new Video("Cours Javascript", 5 , 120);
		System.out.println(video1);
		video1.acheter();
		((Video) video1).afficher();
		System.out.println("---------------------------------");
		
		Disque disc1 = new Disque("Aime la vie", 16, "Maison de disque");
		System.out.println(disc1);
		disc1.acheter();
		disc1.ecouter();
		System.out.println("---------------------------------");
	
		
		

	}

}
