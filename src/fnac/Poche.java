package fnac;

public class Poche extends Livre{
	
	protected String categorie;

	public Poche(String designation, int prix, String isbn, int nbPages, String categorie) {
		super(designation, prix, isbn, nbPages);
		this.categorie = categorie;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		return "Poche [categorie=" + categorie + ", isbn=" + isbn + ", nbPages=" + nbPages + ", designation="
				+ designation + ", prix=" + prix + "]";
	}
	
	
	

}
