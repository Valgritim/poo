package fnac;

public class Article {
	
	protected String designation;
	protected int prix;
	
	public Article(String designation, int prix) {
		super();
		this.designation = designation;
		this.prix = prix;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Article [designation=" + designation + ", prix=" + prix + "]";
	}
	
	public void acheter() {
		 System.out.println("L'article a �t� achet�!");
	}
	
	
	

}
