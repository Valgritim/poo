package fnac;

public class Livre extends Article{
	
	protected String isbn;
	protected int nbPages;

	public Livre(String designation, int prix,String isbn,int nbPages ) {
		super(designation, prix);
		this.isbn = isbn;
		this.nbPages = nbPages;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getNbPages() {
		return nbPages;
	}

	public void setNbPages(int nbPages) {
		this.nbPages = nbPages;
	}

	@Override
	public String toString() {
		return "Livre [isbn=" + isbn + ", nbPages=" + nbPages + ", designation=" + designation + ", prix=" + prix + "]";
	}
	
	

	
}
