package fnac;

public class Video extends Article{
	
	private int duree;

	public Video(String designation, int prix, int duree) {
		super(designation, prix);
		this.duree = duree;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	@Override
	public String toString() {
		return "Video [duree=" + duree + ", designation=" + designation + ", prix=" + prix + "]";
	}
	
	public void afficher() {
		System.out.println("Video en cours de visualisation!");
	}

}
