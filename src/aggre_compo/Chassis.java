package aggre_compo;

public class Chassis {
	
	private int id;
	private String matiere;
	
	
	
	public Chassis(int id, String matiere) {
		super();
		this.id = id;
		this.matiere = matiere;
	}
	public int getId() {
		return id;
	}
	
	//On ne doit pas pouvoir setter un chassis une fois que le vehicule est deja instanci� car on ne peut pas  modifier un chassis dej� cr�� sur une voiture
//	public void setId(int id) {
//		this.id = id;
//	}
	public String getMatiere() {
		return matiere;
	}
//	public void setMatiere(String matiere) {
//		this.matiere = matiere;
//	}
	@Override
	public String toString() {
		return "Chassis [id=" + id + ", matiere=" + matiere + "]";
	}
	
	

}
