package library;

public class Book {
	
	private String editor;
	private String title;
	private String author;
	private int price;
	private String category;
	private String theme;
	
	public Book(String editor, String title, String author, int price, String category, String theme) {
		super();
		this.editor = editor;
		this.title = title;
		this.author = author;
		this.price = price;
		this.category = category;
		this.theme = theme;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	@Override
	public String toString() {
		return "Book [editor=" + editor + ", title=" + title + ", author=" + author + ", price=" + price
				+ ", category=" + category + ", theme=" + theme + "]";
	}
	
		

}
