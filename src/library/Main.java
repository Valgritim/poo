package library;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
					
		List<Book> books = new ArrayList<Book>();
		books.add(new Book("Galimard jeunesse","Le Grufalo ","Audrey Github",7,"roman","premieres lectures"));
		books.add(new Book("Fleuve noir", "La vengeance des Akonides", "Scheer and Darlton",6, "roman", "science-fiction"));
		books.add(new Book("Hachette","Reviens-moi","Harlan Coben", 25, "roman", "suspens"));
		
		books.forEach(System.out::println);
		System.out.println("----------------------");
		
		books.forEach(Book->System.out.println(Book));
		
		System.out.println("\rOuvrages disponibles: ");
		int increment = 1;
		for(Book book: books) {		
			System.out.println(increment + "." + book.getTitle() + " - " + "prix:" + " " + book.getPrice() + "�");
			increment++;
		}
		

	}

}
