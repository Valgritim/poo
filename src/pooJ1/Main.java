package pooJ1;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		Voiture v1 = new Voiture("157VE06","VW","Polo",8,"Limestone");
		System.out.println(v1);
		String couleurPolo = v1.getCouleur();
		System.out.println("La couleur de cette voiture est " + couleurPolo);
		v1.setCouleur("Bleue");
		System.out.println("La couleur de cette voiture est " + v1.getCouleur());
		
		List<Voiture> voitures = new ArrayList<Voiture>();
		voitures.add(new Voiture("251EYG32","Peugeot","5008",10,"noire"));
		voitures.add(new Voiture("157VE06","VW","Polo",8,"Limestone"));
		voitures.add(new Voiture("327FGH89","Renault","Megane",8,"blanche"));
		
		//Java 8:programmation fonctionnelle au lieu de for(etc...)
		
		voitures.forEach(Voiture->System.out.println(Voiture));// equivaut �:
		System.out.println("--------------------------------");
		
		voitures.forEach(System.out::println);
		System.out.println("--------------------------------");
		//Equivaut �:
		for(Voiture v: voitures) {
			System.out.println("voiture:" + v);
		}
		
		//Equivaut �:
		for(int i = 0; i < voitures.size(); i++)
		{
		    System.out.println(voitures.get(i));
		}

	}

}
