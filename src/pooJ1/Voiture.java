package pooJ1;

public class Voiture {

	private String numImmat;
	private String marque;
	private String modele;
	private int puissance;
	private String couleur;
	
	public Voiture(String numImmat, String marque, String modele, int puissance, String couleur) {
		super();
		this.numImmat = numImmat;
		this.marque = marque;
		this.modele = modele;
		this.puissance = puissance;
		this.couleur = couleur;
	}

	public String getNumImmat() {
		return numImmat;
	}

	public void setNumImmat(String numImmat) {
		this.numImmat = numImmat;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getPuissance() {
		return puissance;
	}

	public void setPuissance(int puissance) {
		this.puissance = puissance;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	/**
	 * La methode toString est implicite, on appelle juste l'objet ex: System.out.println(objet1)
	 */
	@Override
	public String toString() {
		return "Voiture [numImmat=" + numImmat + ", marque=" + marque + ", modele=" + modele + ", puissance="
				+ puissance + ", couleur=" + couleur + "]";
	}
	
	
	
	
	
	
}
