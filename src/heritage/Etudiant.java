package heritage;

public class Etudiant extends Personne{
	
	private String niveau;

	public Etudiant(String firstName, String lastName, String dateBirth, Adresse adress, String niveau) {
		super(firstName, lastName, dateBirth, adress);
		this.niveau = niveau;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return "Etudiant [niveau=" + niveau + ", firstName=" + firstName + ", lastName=" + lastName + ", dateBirth="
				+ dateBirth + "]";
	}





	
	
	

}
