package heritage;

public class Personne implements IMiseEnForme{

	protected String firstName;
	protected String lastName;
	protected String dateBirth;
	private Adresse adress;
	private int id;
	private static int count;//Auto incrémentation
	
	public Personne(String firstName, String lastName, String dateBirth, Adresse adress) {
		super();
		this.id = ++count;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateBirth = dateBirth;
		this.adress = adress;
		
		
	}

	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getDateBirth() {
		return dateBirth;
	}



	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}



	public Adresse getAdress() {
		return adress;
	}



	public void setAdress(Adresse adress) {
		this.adress = adress;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "Personne [firstName=" + firstName + ", lastName=" + lastName + ", dateBirth=" + dateBirth + ", adress="
				+ adress + ", id=" + id + "]";
	}

	@Override
	public void afficherNomMajuscule() {
		System.out.println(firstName.toUpperCase());
		
	}

	@Override
	public void afficherPrenomMajuscule() {
		System.out.println(lastName.toUpperCase());
		
	}
	
	
}
