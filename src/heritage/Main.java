package heritage;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
//		Personne s1 = new Etudiant(null, null, null, null);
//		System.out.println(s1);
//		Personne t1 = new Enseignant(null, null, null, 0, null);
//		System.out.println(t1);
//		System.out.println("----------------------------------------------");
//		List<Personne> personnes = new ArrayList<Personne>();
//		
//		personnes.add(new Etudiant("Younes", "Temar", "26/02/2000", "license"));
//		personnes.add(new Enseignant("Damien", "Monchaty", "10/02/1980", 48000, "25252525"));
//		
//		personnes.forEach(System.out::println);
		
//		Etudiant brillant = new Etudiant("Youssef", "Temar", "26/02/2000", "license");
//		brillant.afficherNomComplet();
//		
//		Enseignant topissime = new Enseignant("Damien", "Monchaty", "10/02/1980", 40000, "25252525");
//		topissime.afficherNomComplet();
		
//		Etudiant brillant = new Etudiant("Younes", "Temar", "26/02/2000", "license");
//		brillant.afficherNomMajuscule();
//		brillant.afficherPrenomMajuscule();
		
		//Ici je cr��e une aggr�gation:
		Personne person1 = new Personne("Asma", "Gouja", "25/10/1990", new Adresse("rue des roses", "06210", "Mandelieu"));
		System.out.println(person1);
		Personne person2 = new Personne("Hanen", "Douja", "10/02/1993", new Adresse("rue des ecureuils", "06400", "Cannes"));
		System.out.println(person2);

	}

}
