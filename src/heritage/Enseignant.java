package heritage;

public class Enseignant extends Personne{
	
	private int salary;
	private String recruitDate;

	public Enseignant(String firstName, String lastName, String dateBirth, Adresse adress, int salary,
		String recruitDate) {
		super(firstName, lastName, dateBirth, adress);
		this.salary = salary;
		this.recruitDate = recruitDate;
	}

	public int getsalary() {
		return salary;
	}

	public void setsalary(int salary) {
		this.salary = salary;
	}

	public String getRecruitDate() {
		return recruitDate;
	}

	public void setRecruitDate(String recruitDate) {
		this.recruitDate = recruitDate;
	}

	@Override
	public String toString() {
		return "Enseignant [salary=" + salary + ", recruitDate=" + recruitDate + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", dateBirth=" + dateBirth + "]";
	}




}
	
	


